#!/usr/bin/env python

from setuptools import setup
import json
import os

pkginfo_path = os.path.join(os.path.dirname(__file__),
                            '',
                            'chinaPersonalTaxCalc_info.json')
pkginfo = json.load(open(pkginfo_path))

setup(name=pkginfo['name'],
      version=pkginfo['version'],
      description=pkginfo['description'],
      author=pkginfo['main_author'],
      author_email=pkginfo['main_author_email'],
      url=pkginfo['url'],
      license=pkginfo['license'],
      packages=['chinaPersonalTaxCalc', ],
      package_data={'chinaPersonalTaxCalc': ['chinaPersonalTaxCalc_info.json']},
      install_requires=[],
      requires=[],
      # include_dirs=[numpy.get_include()],
      setup_requires=[],
      tests_require=[],
      #long_description=open('README.md', 'r').read(),
      )
