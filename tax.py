# -*- coding: UTF-8 -*-
# pylint: disable=C0103, C0111, C0326, C1001, W0401, W0612, W0613, W0621

import json
from collections import OrderedDict
import os

pkginfo_path = os.path.join(os.path.dirname(__file__),
                            '',
                            'chinaPersonalTaxCalc_info.json')
pkginfo = json.load(open(pkginfo_path))

userinfo_path = os.path.join(os.path.dirname(__file__),
                            '',
                            'user_info.json')
userinfo = json.load(open(userinfo_path), object_pairs_hook=OrderedDict)

cityinfo_path = os.path.join(os.path.dirname(__file__),
                            '',
                            'city_info.json')
cityinfo = json.load(open(cityinfo_path))

def getUserInfo(info):
    unregular                = info[1]["unregular"]
    bonusyear                = info[1]["bonusyear"]
    housingFundRating        = info[1]["housingFundRating"]
    baseSocialSecurityRating = info[1]["baseSocialSecurityRating"]
    baseHousingFundRating    = info[1]["baseHousingFundRating"]
    medicalInsuranceLevel    = info[1]["medicalInsuranceLevel"]
    settled                  = info[1]["settled"]
    city                     = info[1]["city"]

    return unregular, bonusyear, housingFundRating, baseSocialSecurityRating, baseHousingFundRating, medicalInsuranceLevel, settled, city

def getCityInfo(info, settled):
    averageSalaryLatest                = info["averageSalaryLatest"]
    minimumSalaryLatest                = info["minimumSalaryLatest"]
    taxThreshold                       = info["taxThreshold"]
    
    oldAgeInsuranceRating              = info["oldAgeInsuranceRating"]
    medicalInsuranceRating             = info["medicalInsuranceRating"]
    unemployInsuranceRating            = info["unemployInsuranceRating"]
    maternityInsuranceRating           = info["maternityInsuranceRating"]
    employmentInjuryInsuranceRating    = info["employmentInjuryInsuranceRating"]
    
    oldAgeInsuranceRatingSettled       = info["oldAgeInsuranceRatingSettled"]
    
    if settled == 1:
        oldAgeInsuranceRating = oldAgeInsuranceRatingSettled

    return averageSalaryLatest, minimumSalaryLatest, taxThreshold, \
           oldAgeInsuranceRating, medicalInsuranceRating, unemployInsuranceRating, maternityInsuranceRating, employmentInjuryInsuranceRating

def salaryAfterTax(city, unregular, bonusyear, user_salary, user_subsidies, baseSocialSecurityRating, baseHousingFundRating, \
                   averageSalaryLatest, minimumSalaryLatest, taxThreshold, housingFundRating, \
                   oldAgeInsuranceRating, medicalInsuranceRating, unemployInsuranceRating, \
                   maternityInsuranceRating, employmentInjuryInsuranceRating):
    _user_salary         = user_salary
    user_salary          = _user_salary * unregular
    housingFundSalary    = _user_salary * baseHousingFundRating
    socialSecuritySalary = _user_salary * baseSocialSecurityRating
    salaryBeforeTax      = user_salary + user_subsidies
    minimumSalary        = minimumSalaryLatest
    
    threshold            = taxThreshold
    
    # endowmentInsuranceRating
    _oldAgeInsuranceRating           = {"Personal":oldAgeInsuranceRating[0],            "Company":oldAgeInsuranceRating[1]}
    _medicalInsuranceRating          = {"Personal":medicalInsuranceRating[0],           "Company":medicalInsuranceRating[1]}
    _unemployInsuranceRating         = {"Personal":unemployInsuranceRating[0],          "Company":unemployInsuranceRating[1]}
    _maternityInsuranceRating        = {"Personal":maternityInsuranceRating[0],         "Company":maternityInsuranceRating[1]}
    _employmentInjuryInsuranceRating = {"Personal":employmentInjuryInsuranceRating[0],  "Company":employmentInjuryInsuranceRating[1]}
    _housingFundRating               = {"Personal":housingFundRating[0],                "Company":housingFundRating[1]}
    
    oldAgeInsurance           = {}
    medicalInsurance          = {}
    unemployInsurance         = {}
    maternityInsurance        = {}
    employmentInjuryInsurance = {}
    housingFund               = {}
    
    total5Insurance           = {}
    total6Insurance           = {}
    

    # 2016年社平工资7706，五险一金上限是社评三倍工资
    averageSalary = averageSalaryLatest
    tripleAverageSalary = 3 * averageSalary

    if socialSecuritySalary < tripleAverageSalary:
        if unregular == 1:
            print('#' * 18 + 'Regular' + '#' * 19)
        else:
            print('#' * 17 + 'Unregular' + '#' * 18)
        
        oldAgeInsurance["Personal"]           = housingFundSalary    * _oldAgeInsuranceRating["Personal"]
        medicalInsurance["Personal"]          = socialSecuritySalary * _medicalInsuranceRating["Personal"]
        unemployInsurance["Personal"]         = minimumSalary        * _unemployInsuranceRating["Personal"]
        maternityInsurance["Personal"]        = socialSecuritySalary * _maternityInsuranceRating["Personal"]
        employmentInjuryInsurance["Personal"] = socialSecuritySalary * _employmentInjuryInsuranceRating["Personal"]
        housingFund["Personal"]               = housingFundSalary    * _housingFundRating["Personal"]
        
        total5Insurance["Personal"] = oldAgeInsurance["Personal"] + \
                                      medicalInsurance["Personal"] + \
                                      unemployInsurance["Personal"] + \
                                      maternityInsurance["Personal"] + \
                                      employmentInjuryInsurance["Personal"]
        total6Insurance["Personal"] = total5Insurance["Personal"] + housingFund["Personal"]

        oldAgeInsurance["Company"]           = housingFundSalary    * _oldAgeInsuranceRating["Company"]
        medicalInsurance["Company"]          = socialSecuritySalary * _medicalInsuranceRating["Company"]
        unemployInsurance["Company"]         = minimumSalary        * _unemployInsuranceRating["Company"]
        maternityInsurance["Company"]        = socialSecuritySalary * _maternityInsuranceRating["Company"]
        employmentInjuryInsurance["Company"] = socialSecuritySalary * _employmentInjuryInsuranceRating["Company"]
        housingFund["Company"]               = housingFundSalary    * _housingFundRating["Company"]
        
        total5Insurance["Company"] = oldAgeInsurance["Company"] + \
                                     medicalInsurance["Company"] + \
                                     unemployInsurance["Company"] + \
                                     maternityInsurance["Company"] + \
                                     employmentInjuryInsurance["Company"]
        total6Insurance["Company"] = total5Insurance["Company"] + housingFund["Company"]
        
    else:
        if unregular == 1:
            print('#' * 12 + 'Regular Over Protect' + '#' * 12)
        else:
            print('#' * 11 + 'Unregular Over Protect' + '#' * 11)
        		
        
        housingFundSalary    = tripleAverageSalary
        socialSecuritySalary = tripleAverageSalary
        #totalInsurance = tripleAverageSalary * (
        #    oldAgePersonalRating + medicalPersonalRating + unemployPersonalRating + housingFundPersonalRating)
        #housingFund = tripleAverageSalary * housingFundPersonalRating   #公积金封顶
        #housingFund = salaryBeforeTax * housingFundRating     #公司给补超额公积金部分
        oldAgeInsurance["Personal"]           = housingFundSalary    * _oldAgeInsuranceRating["Personal"]
        medicalInsurance["Personal"]          = socialSecuritySalary * _medicalInsuranceRating["Personal"]
        unemployInsurance["Personal"]         = minimumSalary        * _unemployInsuranceRating["Personal"]
        maternityInsurance["Personal"]        = socialSecuritySalary * _maternityInsuranceRating["Personal"]
        employmentInjuryInsurance["Personal"] = socialSecuritySalary * _employmentInjuryInsuranceRating["Personal"]
        housingFund["Personal"]               = housingFundSalary    * _housingFundRating["Personal"]
        
        total5Insurance["Personal"] = oldAgeInsurance["Personal"] + \
                                      medicalInsurance["Personal"] + \
                                      unemployInsurance["Personal"] + \
                                      maternityInsurance["Personal"] + \
                                      employmentInjuryInsurance["Personal"]
        total6Insurance["Personal"] = total5Insurance["Personal"] + housingFund["Personal"]

        oldAgeInsurance["Company"]           = housingFundSalary    * _oldAgeInsuranceRating["Company"]
        medicalInsurance["Company"]          = socialSecuritySalary * _medicalInsuranceRating["Company"]
        unemployInsurance["Company"]         = minimumSalary        * _unemployInsuranceRating["Company"]
        maternityInsurance["Company"]        = socialSecuritySalary * _maternityInsuranceRating["Company"]
        employmentInjuryInsurance["Company"] = socialSecuritySalary * _employmentInjuryInsuranceRating["Company"]
        housingFund["Company"]               = housingFundSalary    * _housingFundRating["Company"]
        
        total5Insurance["Company"] = oldAgeInsurance["Company"] + \
                                     medicalInsurance["Company"] + \
                                     unemployInsurance["Company"] + \
                                     maternityInsurance["Company"] + \
                                     employmentInjuryInsurance["Company"]
        total6Insurance["Company"] = total5Insurance["Company"] + housingFund["Company"]

    # 纳税额
    payment = salaryBeforeTax - total6Insurance["Personal"] - threshold
    tax = taxRate(payment)

    # 税后工资
    salaryAfterTax = salaryBeforeTax - total6Insurance["Personal"] - tax
    actualIncome = salaryAfterTax + housingFund["Personal"] * 2
    
    __char_nbrs = (int)((44 - len(city))/2)
    print('-' * __char_nbrs + city + '-' * (__char_nbrs+len(city)%2))
    print('Salary is : %d, Subsidies is : %d, Total is : %d' % (user_salary, user_subsidies, user_salary + user_subsidies))
    print('Salary after tax is : %d , Tax of salary is : %d' % (salaryAfterTax, tax))
    print('Actual income Percent is : %.2f %%' % float(actualIncome * 100 / salaryBeforeTax))
    print('Actual income including Housing Fund is : %d ' % actualIncome)
    print('-' * 44)
    print('                                %s \t %s' % ("Personal", "Company"))
    print('OldAge           Insurance is : %d(%d%%) \t %d(%d%%)' % (oldAgeInsurance["Personal"], 100 * _oldAgeInsuranceRating["Personal"], \
                                                                    oldAgeInsurance["Company"], 100 * _oldAgeInsuranceRating["Company"]))
    print('Medical          Insurance is : %-3d(%d%%) \t %-3d(%d%%)' % (medicalInsurance["Personal"], 100 * _medicalInsuranceRating["Personal"], \
                                                                        medicalInsurance["Company"], 100 * _medicalInsuranceRating["Company"]))
    print('Unemploy         Insurance is : %d(%.1f%%) \t %d(%.1f%%)' % (unemployInsurance["Personal"], 100 * _unemployInsuranceRating["Personal"], \
                                                                        unemployInsurance["Company"], 100 * _unemployInsuranceRating["Company"]))
    print('EmploymentInjury Insurance is : %d(%.1f%%) \t %d(%.1f%%)' % (employmentInjuryInsurance["Personal"], 100 * _employmentInjuryInsuranceRating["Personal"], \
                                                                        employmentInjuryInsurance["Company"], 100 * _employmentInjuryInsuranceRating["Company"]))
    print('Maternity        Insurance is : %d(%.1f%%) \t %d(%.1f%%)' % (maternityInsurance["Personal"], 100 * _maternityInsuranceRating["Personal"], \
                                                                        maternityInsurance["Company"], 100 * _maternityInsuranceRating["Company"]))
    print('Up Total         Insurance is : %d \t\t %d' % (total5Insurance["Personal"], total5Insurance["Company"]))
    print('Housing          Fund      is : %d(%d%%) \t %d(%d%%)' % (housingFund["Personal"], 100 * _housingFundRating["Personal"], \
                                                                    housingFund["Company"], 100 * _housingFundRating["Company"]))
    print('Total            Insurance is : %d \t\t %d' % (total6Insurance["Personal"], total6Insurance["Company"]))
    
    print('' * 1)
    if unregular == 1:
        print('Bonusyear Translated Average Salary Per Month is : %d' \
            % (bonusyear*_user_salary/12 - taxRate(bonusyear*_user_salary/12) + actualIncome))
        print('Total     Year               Salary           is : %d' \
            % (12 * (bonusyear*_user_salary/12 - taxRate(bonusyear*_user_salary/12) + actualIncome)))
    print('-' * 44)
    
    return salaryAfterTax

def taxRate(base):
    if base < 0:
        tax = 0
    elif base <= 1500:
        tax = base * 0.03
    elif base > 1500 and base <= 4500:
        tax = base * 0.1 - 105
    elif base > 4500 and base <= 9000:
        tax = base * 0.2 - 555
    elif base > 9000 and base <= 35000:
        tax = base * 0.25 - 1005
    elif base > 35000 and base <= 55000:
        tax = base * 0.3 - 2755
    elif base > 55000 and base <= 80000:
        tax = base * 0.35 - 5505
    elif base > 80000:
        tax = base * 0.45 - 13505

    # print('Tax of salary is : %d' % tax)

    return tax

def run():
    for user in userinfo.items():
        unregular, bonusyear, housingFundRating, baseSocialSecurityRating, baseHousingFundRating, medicalInsuranceLevel, settled, city = getUserInfo(user)
        for level in user[1]["level"]:
            user_salary    = level["user_salary"]
            user_subsidies = level["user_subsidies"]
            averageSalaryLatest, minimumSalaryLatest, taxThreshold, \
                oldAgeInsuranceRating, medicalInsuranceRating, unemployInsuranceRating, \
                maternityInsuranceRating, employmentInjuryInsuranceRating \
                = getCityInfo(cityinfo[city], settled)
            salaryAfterTax(city, unregular, bonusyear, user_salary, user_subsidies, baseSocialSecurityRating, baseHousingFundRating, \
                          averageSalaryLatest, minimumSalaryLatest, taxThreshold, housingFundRating, \
                          oldAgeInsuranceRating, medicalInsuranceRating, unemployInsuranceRating, \
                          maternityInsuranceRating, employmentInjuryInsuranceRating)
            salaryAfterTax(city, 1, bonusyear, user_salary, user_subsidies, baseSocialSecurityRating, baseHousingFundRating, \
                          averageSalaryLatest, minimumSalaryLatest, taxThreshold, housingFundRating, \
                          oldAgeInsuranceRating, medicalInsuranceRating, unemployInsuranceRating, \
                          maternityInsuranceRating, employmentInjuryInsuranceRating)
    pass

if __name__ == '__main__':
    
    run()