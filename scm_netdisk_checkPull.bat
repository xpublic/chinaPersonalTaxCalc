
@echo off
rem -------------------------------------
if not "%~1"=="p" start /min cmd.exe /c %0 p&exit
rem -------------------------------------
cd /d "%~dp0"
set "TOPDIR=%cd%"
title "%~n0"
rem -------------------------------------
if "x%PGM_ROOTDIR%" == "x"  set "  PGM_ROOTDIR=D:/pgm"
if "x%WBK_ROOTDIR%" == "x"  set "WBK_ROOTDIR=D:/wbk"
rem -------------------------------------
pushd "%WBK_ROOTDIR:/=\%"
rem 1 - means origin screen size by 1
rem 2 - means screen size by 2
rem 5 - means full screen size
call "recipes/batches/env.win.init.screen.bat" 1
call "recipes/batches/tpl.title.bat"
popd
rem -------------------------------------
rem setlocal enabledelayedexpansion
rem -------------------------------------
setlocal enabledelayedexpansion
rem -------------------------------------
rem setting variables
rem opts - MSYS CYGWIN
set "WBK_VAR_RUN_OS=MSYS"
rem set "WBK_ENV_CALL_MSY2_CYGWIN_AGAIN=true"
rem set "WBK_VER_MAJOR_PYTHON=2"
rem set "WBK_VAR_HAVE_MINICONDA=false"
rem set "WBK_VAR_OCAML_TYPE=MSYS"

:----------------------------------------
pushd "%WBK_ROOTDIR:/=\%"
call "recipes/batches/env.win.entry.bat"
popd
:----------------------------------------
pushd "%WBK_ROOTDIR:/=\%"
call "recipes/batches/tpl.body.bat"
popd
:----------------------------------------
call "%WBK_ETCDIR%/skel/tpl.body.get.current.dir.bat"
call "%WBK_ETCDIR%/skel/tpl.body.create.desktop.shortcut.bat"

:----------------------------------------
:     SCM Pulling Start
:----------------------------------------
call "%WBK_ETCDIR%/skel/tpl.repo.netdisk.scm.git.pull.bat"
:----------------------------------------
:     SCM Pulling END
:----------------------------------------

rem -------------------------------------
pushd "%WBK_ROOTDIR:/=\%"
call "recipes/batches/tpl.tail.bat"
popd
