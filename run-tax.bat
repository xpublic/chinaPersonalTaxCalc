
@echo off
rem -------------------------------------
if not "%~1"=="p" start /min cmd.exe /c %0 p&exit
rem -------------------------------------
cd /d "%~dp0"
set "TOPDIR=%cd%"
title "%~n0"
rem -------------------------------------
if "x%PGM_ROOTDIR%" == "x"  set "  PGM_ROOTDIR=D:/pgm"
if "x%WBK_ROOTDIR%" == "x"  set "WBK_ROOTDIR=D:/wbk"
if "x%PGMBAK_ROOTDIR%" == "x"  set "  PGMBAK_ROOTDIR=E:/pgm"
rem -------------------------------------

rem -------------------------------------
: setlocal enabledelayedexpansion
rem -------------------------------------
setlocal enabledelayedexpansion
rem -------------------------------------


:----------------------------------------
set "ORIGIN_PATH=%PATH%"
set "MINI_PATH=C:/WINDOWS/system32;C:/WINDOWS;C:/WINDOWS/System32/Wbem"
set "PATH=%MINI_PATH%"
:----------------------------------------
set "MINICONDA_BIT=x64"
set "MINICONDA_VER=2"
set "MINICONDA_ROOT=D:/pgm/DEV/Miniconda/Miniconda2/x64/envs/envWbkPy2"

set "PATH=%MINICONDA_ROOT%;%MINICONDA_ROOT%/Scripts;%PATH%"

if not exist "user_info.json" copy "user_info_default.json" "user_info.json"
python tax.py

goto :eof_with_pause
if "%errorlevel%" == "0" goto :eof_with_exit
rem -------------------------------------
:eof_with_pause
pause
:eof_with_exit
exit

